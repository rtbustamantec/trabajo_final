class Visita
	attr_accessor :codigo, :fecha, :residente, :visitante, :habitacion

	def initialize(codigo, fecha, residente, visitante, habitacion)
		@codigo, @fecha, @residente, @visitante, @habitacion = codigo, fecha, residente, visitante, habitacion
	end
end