class Recibo
	attr_accessor :codigo, :fecha_emision, :monto, :estado, :fecha_pago

	def initialize(codigo, fecha_emision, monto, estado, fecha_pago='')
		@codigo, @fecha_emision, @monto, @estado, @fecha_pago = codigo, fecha_emision, monto, estado, fecha_pago
	end
	
end