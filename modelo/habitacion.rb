class Habitacion
	attr_accessor :numero_habitacion, :piso, :servicios, :residentes, :familiares, :visitantes

	def initialize(numero_habitacion, piso)
		@numero_habitacion, @piso = numero_habitacion, piso
		@servicios = Array.new()
		@residentes = Array.new()
		@familiares = Array.new()
		@visitantes = Array.new()
	end

	def agregar_servicio(servicio)
		@servicios.push(servicio)
	end

	def agregar_residente(residente)
		@residentes.push(residente)
	end

	def agregar_familiar(familiar)
		@familiares.push(familiar)
	end

	def agregar_visitante(visitante)
		@visitantes.push(visitante)
	end

end