require "../modelo/servicio.rb"


class Servicio
	attr_accessor :codigo, :residente, :recibos

	def initialize(codigo, residente)
		@codigo = codigo
		@residente = residente
		@recibos = Array.new()
	end

	def agregar_recibo(recibo)
		@recibos.push(recibo)
	end
end
