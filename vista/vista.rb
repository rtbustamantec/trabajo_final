require 'date'
require "../util/factory.rb"
require "../controlador/reporte.rb"


residente_obj_1 = Factory.crear_persona('R', '46000001', 'Raul Bustamante')
residente_obj_2 = Factory.crear_persona('R', '46000002', 'Martin Campos')
visitante_obj_1 = Factory.crear_persona('V', '47000001', 'Juan Cruz')
visitante_obj_2 = Factory.crear_persona('V', '47000002', 'Marco Cruz')
familiar_obj_1 = Factory.crear_persona('F', '48000001', 'Pero Bustamante')

recibo_1 = Factory.crear_recibo('C', 'R-AGUA-1001', Date.strptime('30-01-2019', '%d-%m-%Y'), 100, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_2 = Factory.crear_recibo('C', 'R-AGUA-1002', Date.strptime('28-02-2019', '%d-%m-%Y'), 130, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_3 = Factory.crear_recibo('C', 'R-LUZ-2001', Date.strptime('30-03-2019', '%d-%m-%Y'), 80, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_4 = Factory.crear_recibo('P', 'R-LUZ-2002', Date.strptime('30-04-2019', '%d-%m-%Y'), 95)

recibo_5 = Factory.crear_recibo('C', 'R-AGUA-1004', Date.strptime('30-01-2019', '%d-%m-%Y'), 250, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_6 = Factory.crear_recibo('C', 'R-AGUA-1005', Date.strptime('28-02-2019', '%d-%m-%Y'), 230, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_7 = Factory.crear_recibo('C', 'R-LUZ-2003', Date.strptime('30-03-2019', '%d-%m-%Y'), 180, Date.strptime('30-01-2019', '%d-%m-%Y'))
recibo_8 = Factory.crear_recibo('C', 'R-LUZ-2004', Date.strptime('30-04-2019', '%d-%m-%Y'), 170, Date.strptime('30-04-2019', '%d-%m-%Y'))

servicio_agua_obj_1 = Factory.crear_servicio('A', 'AGUA-001', residente_obj_1)
servicio_luz_obj_1 = Factory.crear_servicio('L', 'LUZ-001', residente_obj_1)
servicios_seguridad_obj_1 = Factory.crear_servicio('S', 'SEGUIRDAD-001', residente_obj_1)
servicio_areas_comunes_obj_1 = Factory.crear_servicio('C', 'COMUNES-001', residente_obj_1)

servicio_agua_obj_2 = Factory.crear_servicio('A', 'AGUA-002', residente_obj_2)
servicio_luz_obj_2 = Factory.crear_servicio('L', 'LUZ-002', residente_obj_2)
servicios_seguridad_obj_2 = Factory.crear_servicio('S', 'SEGUIRDAD-002', residente_obj_2)
servicio_areas_comunes_obj_2 = Factory.crear_servicio('C', 'COMUNES-002', residente_obj_2)

servicio_agua_obj_1.agregar_recibo(recibo_1)
servicio_agua_obj_1.agregar_recibo(recibo_2)
servicio_luz_obj_1.agregar_recibo(recibo_3)
servicio_luz_obj_1.agregar_recibo(recibo_4)

servicio_agua_obj_2.agregar_recibo(recibo_5)
servicio_agua_obj_2.agregar_recibo(recibo_6)
servicio_luz_obj_2.agregar_recibo(recibo_7)
servicio_luz_obj_2.agregar_recibo(recibo_8)

habitacion_obj_1 = Factory.crear_habitacion('N', 'H001', 1)
habitacion_obj_1.agregar_servicio(servicio_agua_obj_1)
habitacion_obj_1.agregar_servicio(servicio_luz_obj_1)
habitacion_obj_1.agregar_servicio(servicios_seguridad_obj_1)
habitacion_obj_1.agregar_servicio(servicio_areas_comunes_obj_1)

habitacion_obj_2 = Factory.crear_habitacion('N', 'H002', 10)
habitacion_obj_2.agregar_servicio(servicio_agua_obj_2)
habitacion_obj_2.agregar_servicio(servicio_luz_obj_2)
habitacion_obj_2.agregar_servicio(servicios_seguridad_obj_2)
habitacion_obj_2.agregar_servicio(servicio_areas_comunes_obj_2)

habitacion_obj_1.agregar_residente(residente_obj_1)
habitacion_obj_1.agregar_visitante(visitante_obj_1)
habitacion_obj_1.agregar_visitante(visitante_obj_2)
habitacion_obj_1.agregar_familiar(familiar_obj_1)

habitacion_obj_2.agregar_residente(residente_obj_2)

visita_obj_1 = Factory.crear_vista('V', 'VS001', Date.strptime('01-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1, habitacion_obj_1)
visita_obj_2 = Factory.crear_vista('V', 'VS001', Date.strptime('02-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1, habitacion_obj_1)
visita_obj_3 = Factory.crear_vista('V', 'VS001', Date.strptime('03-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1, habitacion_obj_1)
visita_obj_4 = Factory.crear_vista('V', 'VS001', Date.strptime('04-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_5 = Factory.crear_vista('V', 'VS001', Date.strptime('05-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_6 = Factory.crear_vista('V', 'VS001', Date.strptime('06-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_7 = Factory.crear_vista('V', 'VS001', Date.strptime('07-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_8 = Factory.crear_vista('V', 'VS001', Date.strptime('08-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_9 = Factory.crear_vista('V', 'VS001', Date.strptime('09-10-2019', '%d-%m-%Y'), residente_obj_1, visitante_obj_1 , habitacion_obj_1)
visita_obj_10 = Factory.crear_vista('V', 'VS002', Date.strptime('09-10-2019', '%d-%m-%Y'), residente_obj_2, visitante_obj_2 , habitacion_obj_1)


reporte = Reporte.new()
reporte.agregar_vista(visita_obj_1)
reporte.agregar_vista(visita_obj_2)
reporte.agregar_vista(visita_obj_3)
reporte.agregar_vista(visita_obj_4)
reporte.agregar_vista(visita_obj_5)
reporte.agregar_vista(visita_obj_6)
reporte.agregar_vista(visita_obj_7)
reporte.agregar_vista(visita_obj_8)
reporte.agregar_vista(visita_obj_9)
reporte.agregar_vista(visita_obj_10)

reporte.agregar_habitacion(habitacion_obj_1)
reporte.agregar_habitacion(habitacion_obj_2)

# Filtros
puts '------------ FILTRO DE VISITAS POR DNI ----------------------'
reporte.filtrar_visitas_por_dni('47000001')

puts ''
puts '----------- FILTRO DE VISITAS POR FECHA ---------------------'
reporte.filtrar_visitas_por_fecha(Date.strptime('05-10-2019', '%d-%m-%Y'))

puts ''
puts '----------- FILTRO DE VISITAS POR RANGO DE FECHAS -----------'
reporte.filtrar_visitas_por_rango_fechas(Date.strptime('02-10-2019', '%d-%m-%Y'), Date.strptime('04-10-2019', '%d-%m-%Y'))

puts ''
puts '----------- FILTRO DE VISITAS POR # DE HABITACION -----------'
reporte.filtrar_visitas_por_numero_habitacion('H001')

puts ''
puts '----------- FILTRO DE PAGOS DE SERVICIOS POR DNI ------------'
reporte.filtrar_pagos_servicios_por_dni('46000001')

puts ''
puts '----------- FILTRO DE PAGOS DE SERVICIOS POR # HABITACION ------------'
reporte.filtrar_pagos_servicios_por_numero_habitacion('H002')

puts ''
puts '----------- FILTRO DE DEUDORES ------------'
reporte.filtrar_deudores()