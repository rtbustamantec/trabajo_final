require "../util/factory.rb"


class Reporte
	attr_accessor :habitaciones, :vistas

	def initialize()
		@visistas = Array.new
		@habitaciones = Array.new
	end

	def agregar_vista(visita)
		@visistas.push(visita)
	end

	def agregar_habitacion(habitacion)
		@habitaciones.push(habitacion)
	end

	def imprimir_visitas(visitas_filtradas)
		puts "-------------------------------------------------------------"
		puts "DNI\t\tNombre\t\t# Habitación\tFecha"
		puts "-------------------------------------------------------------"
		for visita in visitas_filtradas
			puts "#{visita.visitante.dni}\t#{visita.visitante.nombre}\t#{visita.habitacion.numero_habitacion}\t\t#{visita.fecha.to_s}"
		end
		puts "-------------------------------------------------------------"
	end

	def imprimir_pagos_recibos(recibos_filtrados)
		puts "-------------------------------------------------------------"
		puts "Codigo\t\tDNI\t\tNombre\t\t\t# Habitación\tEstado\t\tF. Emision\tF. Pago"
		puts "-------------------------------------------------------------"
		for recibo_filtrado in recibos_filtrados
			habitacion, servicio, recibo = recibo_filtrado[0], recibo_filtrado[1], recibo_filtrado[2]
			puts "#{recibo.codigo}\t#{servicio.residente.dni}\t#{servicio.residente.nombre}\t\t#{habitacion.numero_habitacion}\t\t#{recibo.estado}\t#{recibo.fecha_emision.to_s}\t#{recibo.fecha_pago.to_s}"
		end
		puts "-------------------------------------------------------------"
	end

	def filtrar_visitas_por_dni(dni)
		visitas_filtradas = []
		for visita in @visistas
			if visita.visitante.dni == dni
				visitas_filtradas.push(visita)
			end
		end
		imprimir_visitas(visitas_filtradas)
	end

	def filtrar_visitas_por_fecha(fecha)
		visitas_filtradas = []
		for visita in @visistas
			if visita.fecha == fecha
				visitas_filtradas.push(visita)
			end
		end
		imprimir_visitas(visitas_filtradas)
	end

	def filtrar_visitas_por_rango_fechas(fecha_inicial, fecha_final)
		visitas_filtradas = []
		for visita in @visistas
			if visita.fecha.between?(fecha_inicial, fecha_final)
				visitas_filtradas.push(visita)
			end
		end
		imprimir_visitas(visitas_filtradas)
	end

	def filtrar_visitas_por_numero_habitacion(numero_habitacion)
		visitas_filtradas = []
		for visita in @visistas
			if visita.habitacion.numero_habitacion == numero_habitacion
				visitas_filtradas.push(visita)
			end
		end
		imprimir_visitas(visitas_filtradas)
	end

	def filtrar_pagos_servicios_por_dni(dni)
		recibos_filtrados = []
		for habitacion in @habitaciones
			for servicio in habitacion.servicios
				if servicio.residente.dni == dni
					for recibo in servicio.recibos
						if recibo.estado == 'CANCELADO'
							recibos_filtrados.push([habitacion, servicio, recibo])
						end
					end
				end
			end
		end
		imprimir_pagos_recibos(recibos_filtrados)
	end

	def filtrar_pagos_servicios_por_numero_habitacion(numero_habitacion)
		recibos_filtrados = []
		for habitacion in @habitaciones
			if habitacion.numero_habitacion == numero_habitacion
				for servicio in habitacion.servicios
					for recibo in servicio.recibos
						if recibo.estado == 'CANCELADO'
							recibos_filtrados.push([habitacion, servicio, recibo])
						end
					end
				end
			end
		end
		imprimir_pagos_recibos(recibos_filtrados)
	end

	def filtrar_deudores()
		recibos_filtrados = []
		for habitacion in @habitaciones
			for servicio in habitacion.servicios
				for recibo in servicio.recibos
					if recibo.estado == 'PENDIENTE'
						recibos_filtrados.push([habitacion, servicio, recibo])
					end
				end
			end
		end
		imprimir_pagos_recibos(recibos_filtrados)
	end
end