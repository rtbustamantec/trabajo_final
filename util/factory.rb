require "../modelo/residente.rb"
require "../modelo/familiar.rb"
require "../modelo/visitante.rb"

require "../modelo/agua.rb"
require "../modelo/luz.rb"
require "../modelo/seguridad.rb"
require "../modelo/areas_comunes.rb"

require "../modelo/recibo.rb"

require "../modelo/habitacion.rb"

require "../modelo/visita.rb"


class Factory

	def self.crear_persona(tipo, *ar)
		case tipo
		when 'R'
			return Residente.new(ar[0], ar[1])
		when 'F'
			return Familiar.new(ar[0], ar[1])
		when 'V'
			return Visitante.new(ar[0], ar[1])
		end
	end

	def self.crear_servicio(tipo, *ar)
		case tipo
		when 'A'
			return ServicioAgua.new(ar[0], ar[1])
		when 'L'
			return ServicioLuz.new(ar[0], ar[1])
		when 'S'
			return ServicioSeguridad.new(ar[0], ar[1])
		when 'C'
			return ServicioAreasComunes.new(ar[0], ar[1])
		end
    end 
    
    def self.crear_recibo(estado, *ar)
		case estado
		when 'C'
			return Recibo.new(ar[0], ar[1], ar[2], 'CANCELADO', ar[4])
		when 'P'
			return Recibo.new(ar[0], ar[1], ar[2], 'PENDIENTE', ar[4])
		end
    end 

    def self.crear_habitacion(tipo, *ar)
		case tipo
		when 'N'
			return Habitacion.new(ar[0], ar[1])
		end
    end 

    def self.crear_vista(tipo, *ar)
        case tipo
		when 'V'
			return Visita.new(ar[0], ar[1], ar[2], ar[3], ar[4])
		end
    end

end